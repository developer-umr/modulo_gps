package intranet.client.localizacion;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private TextView t1, t2, t3;
    private LocationManager locationManager;
    private static final String[] estados = {"Fuera de servicio",
            "Temporalmenta inhabilitasdo", "Disponible"};

    private Double[] coordEmpresa = {17.0753762, -96.7198703, 20.0};
    private Double[] coord1 = {17.075126, -96.719722};
    private Double[] coordMontoya = {17.069449, -96.75949};
    private Double[] coordOXXO = {17.075533, -96.719475};
    private String mejorProvedor;

    double latitud = 0.0, longitud = 0.0;

    Location localizacionA;
    Location localizacionB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager =(LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        localizacionA = new Location("mi ubicacion");
        localizacionB = new Location("geocerca empresa");
        iniciarCoordenadasGeocercas();
        t1 = (TextView) findViewById(R.id.tvLatitud);
        t2 = (TextView) findViewById(R.id.tvLongitud);
        t3 = (TextView) findViewById(R.id.tvDistancia);

        Location location = null;

        List<String> provedores = locationManager.getAllProviders();
        for (String provider : provedores) {
            Log.d(null, "___________________________----" + provider);
        }

        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setCostAllowed(false);

        mejorProvedor = locationManager.getBestProvider(criteria, false);

        Log.d(null, "__________________Mejor proveedor: " + mejorProvedor);
        if (mejorProvedor != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d(null,"__________________No tiene los permisos");
            }
            location=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            pruebas(location);
            location=locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            pruebas(location);
            location=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            pruebas(location);
            location = locationManager.getLastKnownLocation(mejorProvedor);
            pruebas(location);
        }



        //        LocationProvider providerGPS =locationManager.getProvider ( LocationManager.GPS_PROVIDER );
        /*
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Log.d(null, "_______________________________________________provider: GPS_PROVIDER");
        } else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Log.d(null, "_______________________________________________provider: NETWORK_PROVIDER");
        }*/

        //onLocationChanged(location);
    }

    private void pruebas(Location location){
        if(location!=null) {
            localizacionA.setLatitude(latitud);
            localizacionA.setLongitude(longitud);
            Log.d(null, "\nprueba proveerdor: " + location.getProvider() + "\nlatitud: " + location.getLatitude() + "" +
                    "\nLongitud:" + location.getLongitude() + "\nDesvio: " + location.getAccuracy() + "\nDistancia: "+calcularDistanci2(localizacionA,localizacionB));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationManager =(LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!gpsEnabled) {
            // Construccion del intent que solicitara al usuario
            enableLocationSettings();
        }
    }

    private void enableLocationSettings() {
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(settingsIntent);
    }

    private void iniciarCoordenadasGeocercas(){
        //supuesto: obtener las geocercas, crear objetos Location de cada geocerca
        localizacionB.setLatitude(17.0753762);
        localizacionB.setLongitude(-96.7198703);
    }
    /***
     *
     * @param geoLat latitud obtendida de una geocerca guardada
     * @param geoLong longituf obtenida de una geocerca guardada
     * @param geoR radio obtenido de una geocerca guardadd
     * @return devuelve la distancia en metros
     */
    private double calcularDistancia(Double mLat, Double mLong, Double geoLat, Double geoLong, Double geoR) {
        boolean incidencia = false;
        double radianes = Math.PI / 180;
        double distanciaMetros = 0;
        //comparar coordenadas
        if (mLat != null && mLong != null) {
            double dlat = mLat - geoLat;
            double dlon = mLong - geoLong;
            double radioT = 6372.795477592;
            double a = Math.pow(Math.sin(radianes * dlat / 2), 2) + Math.cos(radianes * mLat) * Math.cos(radianes * geoLat) * Math.pow(radianes * Math.sin(dlon / 2), 2);
            double distancia = 2 * radioT * Math.asin(Math.sqrt(a));
            distanciaMetros = distancia * 1000;
        }
        return distanciaMetros;
    }

    private double calcularDistanci2(Location locationA, Location locationB) {
        double distancia = locationA.distanceTo(locationB);
        return distancia;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            longitud = location.getLongitude();
            latitud = location.getLatitude();
            t1.setText("Longitud: " + longitud);
            t2.setText("Latitud: " + latitud);
            //Tomar coordenadas
            localizacionA.setLatitude(latitud);
            localizacionA.setLongitude(longitud);

            //pruebas
            Log.d(null, "____________________Distancia con el método 1: " + calcularDistancia(latitud, longitud, coordEmpresa[0], coordEmpresa[1], coordEmpresa[2]));
            Log.d(null, "_____________________Distancia con el metodo distanceTo: " + calcularDistanci2(localizacionA, localizacionB));
            t3.setText("distancia metodo1: " + calcularDistancia(latitud, longitud, coordEmpresa[0], coordEmpresa[1], coordEmpresa[2]) + "\n" + "distancia metodo2: " + calcularDistanci2(localizacionA, localizacionB) + "" +
                    "\n" + "desviacion en metros getAccuracy=" + location.getAccuracy());
            Log.d(null, "_____________________________________________________________Desviacion: " + location.getAccuracy());
            Log.d(null, "_____________________________________________________________Desviacion: " + location.getAccuracy());
            Log.d(null, "_____________________________________________________________Desviacion: " + location.getAccuracy());
            Log.d(null, "_____________________________________________________________Desviacion: " + location.getAccuracy());
            Log.d(null, "_____________________________________________________________Desviacion: " + location.getAccuracy());
            Log.d(null, "_____________________________________________________________Desviacion: " + location.getAccuracy());
        }
    }

    /** Register for the updates when Activity is in foreground */
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //hacer algo
        }
        locationManager.requestLocationUpdates(mejorProvedor, 20000, 1, this);
    }

    /** Stop the updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void imprimirProvedor(String provider) {
        LocationProvider info = locationManager.getProvider(provider);
        Log.d(null,"___________mensaje: "+info.toString());
    }

    private void imprimirLocalizacion(Location location) {
        if (location == null)
            Log.d(null,"_________Location[Desconocido]:");
        else
            Log.d(null,"_________Location[conocido]: "+location.toString());
    }
    @Override
    public void onStatusChanged(String provedor, int status, Bundle extras) {
        Log.d(null, "Estatus cambiado: "+"\nProvedor: " + provedor + ", estado="
                + estados[status] + ", Extras=" + extras);
    }

    @Override
    public void onProviderEnabled(String provedor) {
        Log.d(null,"_________________________Provider Enables: " + provedor);
    }

    @Override
    public void onProviderDisabled(String provedor) {
        Log.d(null,"_________________________________Provider Disabled: " + provedor);
    }
}
